# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2012 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import osv, fields
import math
from tools.translate import _

class res_partner(osv.osv):

    _inherit = 'res.partner'

    _columns = {
        'ean13': fields.char('EAN Code', size=12, required=False, help="Type here numbers corresponding to COUNTRY + COMPANY EAN13 code (i.e. 9310779)"),
    }

    def _check_ean_digit(self, cr, uid, ids):
        for partner in self.browse(cr, uid, ids):
            if partner.ean13 and not partner.ean13.isdigit():
                return False
        return True

    _constraints = [(_check_ean_digit, _('Error: EAN13 code for the company must be numeric, 8 digits long.'), ['ean13']), ]

res_partner()




class product_product(osv.osv):

    _inherit = 'product.product'

    def get_next_ean13(self, cr, uid, ids, context=None):
        # Obtains next EAN13 sequence number. Action works only if it doesn't exist a previous EAN13 in this product
        # Product's EAN13 code composition: part1 + part2 + dc
        # part1 = COUNTRY_EAN + PARTNER_EAN, which are stored together as the company's partner EAN field
        # part2 = Product's EAN13 sequence number
        curr_company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        if not curr_company.partner_id or not curr_company.partner_id.ean13:
            raise osv.except_osv(_('Error !'), _('Cannot generate EAN13 code! Check that the current company\'s partner has a valid EAN13 code.'))

        product = self.pool.get('product.product').browse(cr, uid, ids[0], context=context)
        if not product.ean13:
            oddsum = 0
            evensum = 0
            total = 0
            ean_part1 = curr_company.partner_id.ean13
            ean_part2 = self.pool.get('ir.sequence').get(cr, uid, 'ean13')
            ean_without_dc = str(ean_part1) + str(ean_part2)
            reverse_ean_without_dc = ean_without_dc[::-1]
            if len(reverse_ean_without_dc) <> 12:
                return False
            try:
                int(reverse_ean_without_dc)
            except:
                return False
            for i in range(len(reverse_ean_without_dc)):
                if not i % 2:
                    oddsum += int(reverse_ean_without_dc[i])
                else:
                    evensum += int(reverse_ean_without_dc[i])
            total = (oddsum * 3) + evensum
            dc = int(10 - math.ceil(total % 10.0)) % 10
            finalean = str(ean_without_dc) + str(dc)
            self.write(cr, uid, ids, {'ean13': str(finalean)}, context=context)
        return True

product_product()
