# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2012 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from osv import fields, osv
from tools.translate import _

class product_ean_generator_installer(osv.osv_memory):
    _name = 'product.ean.generator.installer'
    _inherit = 'res.config.installer'

    _columns = {
        'company_digits': fields.integer('Company digits number', help="Number of digits reserved to identify your company's EAN13 code. This must exclude product\'s number of digits, plus the control digit."),
        'company_ean13': fields.char('EAN Code', size=12, help="Type here numbers corresponding to YOUR COUNTRY + YOUR COMPANY'S RESERVED EAN13 code (i.e. 9310779)"),
        'product_digits': fields.integer('Product digits number', help="Number of digits reserved to the product's EAN13. This will be the size in digits of the product's EAN13 sequence number (do not include control digit)."),
    }

    _defaults = {
        'company_digits': lambda *a: 10,
        'product_digits': lambda *a: 2,
    }

    def _check_ean_digit(self, cr, uid, ids):
        for cur in self.browse(cr, uid, ids):
            if cur.company_ean13 and not cur.company_ean13.isdigit():
                return False
        return True

    def _check_digits_nonzero(self, cr, uid, ids):
        for cur in self.browse(cr, uid, ids):
            if cur.company_digits <= 0 or cur.product_digits <= 0:
                return False
        return True

    def _check_ean_length(self, cr, uid, ids):
        for cur in self.browse(cr, uid, ids):
            if cur.company_digits and len(cur.company_ean13) != cur.company_digits:
                return False
        return True

    _constraints = [(_check_ean_digit, _('Error: EAN13 code for the company must be numeric.'), ['company_ean13']),
                    (_check_ean_length, _('Error: Company\'s EAN13 code length must be equal to Company\'s EAN13 reserved digits !'), ['company_ean13']),
                    (_check_digits_nonzero, _('Error: Number of digits for company and product code cannot be zero or negative !'), ['company_digits', 'product_digits']), ]

    def create_sequence(self, cr, uid, vals, context=None):
        seq_pool = self.pool.get('ir.sequence')
        seq_typ_pool = self.pool.get('ir.sequence.type')

        name = vals['name']
        code = vals['code'].lower()
        padding = vals['padding']

        types = {
            'name': name,
            'code': code
        }
        seq_typ_pool.create(cr, uid, types)

        seq = {
            'name': name,
            'code': code,
            'active': True,
            'prefix': '',
            'suffix': '',
            'padding': padding,
            'number_increment': 1
        }
        if 'company_id' in vals:
            seq['company_id'] = vals['company_id']
        return seq_pool.create(cr, uid, seq)

    def execute(self, cr, uid, ids, context=None):
        data = self.read(cr, uid, ids, context=context)
        #TODO: Maybe add company field to the config wizard?
        curr_company = self.pool.get('res.users').browse(cr, uid, uid, context=context).company_id
        if not data or not curr_company:
            return {}

        if data[0]['company_digits'] + data[0]['product_digits'] != 12:
            raise osv.except_osv(_('Error !'), _('Company and product\'s reserved number of digits must sum 12 ! Please check both fields.'))

        # Save company's EAN13 code into its partner's ean13 field
        partner_obj = self.pool.get('res.partner')
        partner_obj.write(cr, uid, [curr_company.partner_id.id], {'ean13': data[0]['company_ean13']})

        # Create product's EAN13 sequence with product's number of digits (create_sequence method extracted from account/account.py)
        self.create_sequence(cr, uid, {'name': 'EAN13',
                                       'code': 'ean13',
                                       'padding': data[0]['product_digits'],
                                       'company_id': curr_company.id,
                                       }, context)

        return {}

product_ean_generator_installer()
